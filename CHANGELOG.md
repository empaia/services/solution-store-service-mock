# 0.1.10

* added cors_allow_origins to settings again and made allow_credentials also configurable via cors_allow_credentials

# 0.1.9

* allow all origins, which is now possible because frontends do no more use client credentials (instead they explicitly 
use an authorization header)

# 0.1.8

* return 404 instead of 500 when organization or app does not exist

# 0.1.7

* `/alive` endpoint added

# 0.1.6

* storage dir setting

# 0.1.5

* writing inmemory db to json file and restoring it upon restart

# 0.1.4

* custom dev routes to add apps and organizations

# 0.1.3

* added route GET /api/v0/aim/by_app_id/{app_id} (required by JES)

# 0.1.2

* added values for mocked apps to app.aim.namespace

# 0.1.0

* init

# 0.1.1

* added routes required by WBS 0.3.9

# 0.1.0

* init
