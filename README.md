# Solution Store Service Mock

For developement and testing purpose.


## Dev Setup

* install docker
* install docker-compose
* install poetry
* clone solution-store-service-mock

Then run the following commands to create a new virtual environment and install project dependencies.

```bash
sudo apt update
sudo apt install python3-venv python3-pip
cd solution-store-service-mock
python3 -m venv .venv
source .venv/bin/activate
poetry install
```

### Set Environment

Set environment variables in a `.env` file.

```bash
cp sample.env .env  # edit .env if necessary
```

### Run

Start services using `docker-compose`.

```bash
docker-compose up --build -d
```

Or start services with uvicorn

```bash
uvicorn --host=0.0.0.0 --port=10010 --workers=1 --reload  solution_store_service_mock.app:app
```

### Stop and Remove

```bash
docker-compose down
```

### Code Checks

Check your code before committing.

* always format code with `black` and `isort`
* check code quality using `pycodestyle` and `pylint`
  * `black` formatting should resolve most issues for `pycodestyle`

```bash
isort .
black .
pycodestyle solution_store_service_mock
pylint solution_store_service_mock
```
