from importlib.metadata import version

__version__ = version("solution-store-service-mock")
