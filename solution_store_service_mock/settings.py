from typing import Set

from pydantic import BaseSettings


class Settings(BaseSettings):
    debug: bool = False
    cors_allow_credentials: bool = False
    cors_allow_origins: Set[str] = None
    api_v1_integration: str = None
    root_path: str = None
    disable_openapi: bool = False
    storage_dir_path: str = "/data"

    class Config:
        env_file = ".env"
        env_prefix = "sssm_"
