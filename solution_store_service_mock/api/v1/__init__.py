from .routes_aaa import add_routes_aaa
from .routes_server import add_routes_server
from .routes_sss import add_routes_sss


def add_routes_v1(app):
    add_routes_aaa(app)
    add_routes_sss(app)
    add_routes_server(app)
