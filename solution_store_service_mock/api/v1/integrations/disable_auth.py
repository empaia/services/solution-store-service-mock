from fastapi import Depends


class DisableAuth:
    def __init__(self, settings, logger):
        self.settings = settings
        self.logger = logger

    def global_depends(self):
        return Depends()
