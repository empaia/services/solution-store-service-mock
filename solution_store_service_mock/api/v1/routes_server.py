from ... import __version__


def add_routes_server(app):
    @app.get(
        "/alive",
        tags=["server"],
    )
    async def _():
        return {"status": "ok", "version": __version__}
