import json

from fastapi.responses import JSONResponse

from ...singletons import ORGANIZATIONS_BY_ID, api_v1_integration, organizations_file_path


def add_routes_aaa(app):
    @app.post(
        "/api/custom-mock/orgnaization",
        tags=["aaa-custom-mock"],
    )
    async def _(
        organization: dict,
        _=api_v1_integration.global_depends(),
    ):
        ORGANIZATIONS_BY_ID[str(organization["organization_id"])] = organization
        with organizations_file_path.open("w") as f:
            json.dump(ORGANIZATIONS_BY_ID, f)
        return

    @app.get(
        "/api/organization/by_id/{organization_id}",
        tags=["aaa-organization-controller"],
    )
    async def _(
        organization_id: int,
        _=api_v1_integration.global_depends(),
    ):
        if str(organization_id) in ORGANIZATIONS_BY_ID:
            return ORGANIZATIONS_BY_ID[str(organization_id)]
        else:
            return JSONResponse(status_code=404, content={"detail": "organization does not exist"})
