import json

from fastapi.responses import JSONResponse

from ...singletons import APPS_BY_ID, api_v1_integration, apps_file_path


def add_routes_sss(app):
    @app.post(
        "/api/v0/custom-mock/app",
        tags=["sss-custom-mock"],
    )
    async def _(
        app: dict,
        _=api_v1_integration.global_depends(),
    ):
        APPS_BY_ID[app["id"]] = app
        with apps_file_path.open("w") as f:
            json.dump(APPS_BY_ID, f)
        return

    @app.get(
        "/api/v0/app/list",
        tags=["sss-app-controller"],
    )
    async def _(
        _=api_v1_integration.global_depends(),
    ):
        app_list = [APPS_BY_ID[k] for k in APPS_BY_ID]
        return app_list

    @app.get(
        "/api/v0/app/{app_id}",
        tags=["sss-app-controller"],
    )
    async def _(
        app_id: str,
        _=api_v1_integration.global_depends(),
    ):
        if str(app_id) in APPS_BY_ID:
            return APPS_BY_ID[app_id]
        else:
            return JSONResponse(status_code=404, content={"detail": "app does not exist"})

    @app.get(
        "/api/v0/aim/by_app_id/{app_id}",
        tags=["sss-app-controller"],
    )
    async def _(
        app_id: str,
        _=api_v1_integration.global_depends(),
    ):
        if str(app_id) in APPS_BY_ID:
            return APPS_BY_ID[app_id]["aim"]
        else:
            return JSONResponse(status_code=404, content={"detail": "app does not exist"})
