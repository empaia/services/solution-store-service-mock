import json
from logging import DEBUG, getLogger
from pathlib import Path

from .api_integrations import get_api_v1_integration
from .settings import Settings

logger = getLogger("uvicorn")
settings = Settings()

if settings.debug:
    logger.level = DEBUG

api_v1_integration = get_api_v1_integration(settings, logger)

apps_file_path = Path(settings.storage_dir_path, "apps.json")
organizations_file_path = Path(settings.storage_dir_path, "orgas.json")

APPS_BY_ID = {}
ORGANIZATIONS_BY_ID = {}

try:
    with apps_file_path.open() as f:
        APPS_BY_ID = json.load(f)
except FileNotFoundError:
    pass

try:
    with organizations_file_path.open() as f:
        ORGANIZATIONS_BY_ID = json.load(f)
except FileNotFoundError:
    pass
